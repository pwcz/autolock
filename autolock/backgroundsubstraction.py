import cv2
import numpy as np

def nothing(x):
    pass

cv2.createTrackbar('R','image',0,255,nothing)
cv2.createTrackbar('G','image',0,255,nothing)


class BackgroundSubstraction:

    def __init__(self,video):
        self.cap = cv2.VideoCapture(video)
        self.kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(10,10))
        self.fgbg = cv2.createBackgroundSubtractorMOG2()

    def substract_background(self):
        self.fgbg.setHistory(5)
        self.fgbg.setDetectShadows(True)

        while(1):
            ret, frame = self.cap.read()
            avg1 = np.float32(frame)
            avg2 = np.float32(frame)
            if not ret:
                break
            #frameCopy = copy.deepcopy(frame)
            #gray = cv2.cvtColor(_image[self.ly1:self.ly2,self.lx1:self.lx2], cv2.COLOR_BGR2GRAY)
            fgmask = self.fgbg.apply(frame)
            #gray = cv2.cvtColor(fgmask, cv2.COLOR_BGR2GRAY)
            dilation = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, self.kernel)
            """
            r = cv2.getTrackbarPos('R','image')
            g = cv2.getTrackbarPos('G','image')
            edges = cv2.Canny(fgmask,r,g,apertureSize = 3)
            dilation = cv2.morphologyEx(edges, cv2.MORPH_OPEN, self.kernel)
            contours = cv2.findContours(dilation,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)[1]
            print(len(contours))
            cont  = cv2.drawContours(frame, contours, -1, (0,255,0), 3)

            cv2.imshow('gray',edges)
            #edges = cv2.Canny(gray,100,200,apertureSize = 3)

            #cv2.imshow('contours',cont)
            cv2.imshow('frame',fgmask)
            """
            _,contours, _ = cv2.findContours(dilation,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
            frame  = cv2.drawContours(frame, contours, -1, (0,255,0), 3)
            cv2.imshow('frame',frame)
            cv2.imshow('fgmask',fgmask)
            cv2.imshow('dilatio',dilation)
            if cv2.waitKey(30) & 0xFF == ord('q'):
                break

    def close_view(self):
        self.cap.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":

    video_to_substract=0
    video=BackgroundSubstraction(video_to_substract)
    video.substract_background()
    video.close_view()