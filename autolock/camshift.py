import numpy as np
import cv2
import copy

class CamShift:
    def __init__(self,):
        pass

    def prepare(self,frame,_r, _h, _c, _w):
        self.r = _r
        self.h = _h
        self.c = _c
        self.w = _w
        # setup initial location of window
        #r,h,c,w = 120,401,512,291 # simply hardcoded the values
        self.track_window = (self.c, self.r, self.w, self.h)

        # set up the ROI for tracking
        self.roi = frame[self.r:self.r+self.h, self.c:self.c+self.w]
        self.hsv_roi = cv2.cvtColor(self.roi, cv2.COLOR_BGR2HSV)
        self.mask = cv2.inRange(self.hsv_roi, np.array((0., 60.,32.)), np.array((180.,255.,255.)))
        self.roi_hist = cv2.calcHist([self.hsv_roi],[0],self.mask,[180],[0,180])
        cv2.normalize(self.roi_hist,self.roi_hist,0,255,cv2.NORM_MINMAX)
        # Setup the termination criteria, either 10 iteration or move by atleast 1 pt
        self.term_crit = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 )

    def fallow(self, frame, debug=False):
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        dst = cv2.calcBackProject([hsv],[0],self.roi_hist,[0,180],1)
        # apply meanshift to get the new location
        ret, self.track_window = cv2.CamShift(dst, self.track_window, self.term_crit)
        pts = cv2.boxPoints(ret)

        pts = np.int0(pts)
        minX,minY,maxX,maxY = [frame.shape[1], frame.shape[0], 0., 0.]
        for x in pts:
            #print( x)
            if minX > x[0] : minX = x[0]
            if maxX < x[0] : maxX = x[0]
            if minY > x[1] : minY = x[1]
            if maxY < x[1] : maxY = x[1]

        CircX = int((maxX+minX)/2)
        CircY = int((minY+maxY)/2)

        if debug:
            img2 = cv2.polylines(frame,[pts],True, 255,2)
            cv2.circle(img2,(CircX,CircY), 5, (0,0,255), -1)
            cv2.imshow('img2',img2)


        #print(CircY,CircX)

        return CircX,CircY


def main():
    cap = cv2.VideoCapture('krotki.webm')
    engine = CamShift()
    # take first frame of the video
    ret,frame = cap.read()
    engine.prepare(frame,120,401,512,291)

    while(1):

        ret ,frame = cap.read()
        if ret == True:


            kolko = engine.fallow(frame,debug=False)
            # Draw it on image

            #img2 = cv2.polylines(frame,[pts],True, 255,2)
            cv2.circle(frame,(kolko[0],kolko[1]), 5, (0,0,255), -1)
            cv2.imshow('img2',frame)


            k = cv2.waitKey(60) & 0xff

            if cv2.waitKey(0) & 0xFF == ord('q'):
                break

        else:
            break

    cv2.destroyAllWindows()
    cap.release()



if __name__ == "__main__":
    main()




