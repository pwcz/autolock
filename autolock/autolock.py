from facedetection import FaceDetection
import cv2
from camshift import CamShift
import copy


class autolock:

    def __init__(self,roi):
        self.area = roi

    def checkPresence(self, objX,objY):
        if (objX > self.area[0] and objX < self.area[1] and objY > self.area[2] and objY < self.area[3]):
            return False
        else:
            return True

def test():
    cap = cv2.VideoCapture(0)
    engine = FaceDetection()
    objTrack = CamShift()
    roiFrame = autolock([273,1101,26,585])
    while (True):
        ret, frame = cap.read()
        backupframe = copy.deepcopy(frame)
        FallowBool = False
        if ret == True:
            if not FallowBool:
                returnCode = engine.detect_face(frame)
                if returnCode is not False:
                    #x, y, w, h
                    #_r, _h, _c, _w
                    objTrack.prepare(frame, returnCode[1], returnCode[3], returnCode[0], returnCode[2])
                    FallowBool = True

            if FallowBool:
                [x, y] = objTrack.fallow(frame,debug=True)
                objTrackBool = roiFrame.checkPresence(x,y)
                enginBool = roiFrame.checkPresence(returnCode[0]+returnCode[2]/2,returnCode[1]+returnCode[3]/2)
                if objTrackBool or enginBool:
                    cv2.rectangle(backupframe, (roiFrame.area[0], roiFrame.area[2]), (roiFrame.area[1], roiFrame.area[3]), (0, 255, 0), 2)
                    cv2.imshow('DETECT!!',backupframe)

            if cv2.waitKey(10) & 0xFF == ord('q'):
                break
        else:
            break


if __name__ == "__main__":
    test()
