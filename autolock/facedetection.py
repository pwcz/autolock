import cv2

class FaceDetection:

    def __init__(self):
        self.faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

    def detect_face(self,frame,debug=False):
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            faces = self.faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags = cv2.CASCADE_SCALE_IMAGE
        )
            if len(faces) != 0:
                [x, y, w, h] = faces[len(faces)-1]
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
                if debug:
                    cv2.imshow("Faces found", frame)
                return [x, y, w, h]
            else:
                return False


def main():
    cap = cv2.VideoCapture('krotki.webm')
    engine = FaceDetection()
    while(1):
        ret ,frame = cap.read()
        if ret == True:
            engine.detect_face(frame,debug=True)
            if cv2.waitKey(10) & 0xFF == ord('q'):
                break
        else:
            break
    cv2.destroyAllWindows()
    cap.release()

if __name__ == "__main__":
    main()