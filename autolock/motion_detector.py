import cv2

camera = cv2.VideoCapture('krotki.webm')

class MotionDetection:

    def __init__(self):
        self.firstFrame = None
        self.kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (10, 10))

    def detect(self,frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)

        # if the first frame is None, initialize it
        if self.firstFrame is None:
            self.firstFrame = gray
            return False

        # compute the absolute difference between the current frame and
        # first frame
        frameDelta = cv2.absdiff(self.firstFrame, gray)
        thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

        # dilate the thresholded image to fill in holes, then find contours
        # on thresholded image
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, self.kernel)
        thresh = cv2.dilate(thresh, None, iterations=2)
        thresh = cv2.dilate(thresh,self.kernel,iterations = 1)

        cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)[1]
        detect = False
        # loop over the contours
        for c in cnts:
            # if the contour is too small, ignore it
            if cv2.contourArea(c) < 30000:
                continue

            # compute the bounding box for the contour, draw it on the frame,
            # and update the text
            (x, y, w, h) = cv2.boundingRect(c)
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            detect = True

        cv2.imshow("Security Feed", frame)
        if detect:
            return [x, y, w, h]
        else:
            return False

def main():
    cap = cv2.VideoCapture(0)
    engine = MotionDetection()
    #engine.setRoi([273, 26], [1101, 585])

    while (True):
        ret, frame = cap.read()
        if ret == True:
            returnCode = engine.detect(frame)
            if returnCode is not False:
                print(returnCode)
                #cv2.waitKey(0) & 0xFF == ord('q')
            if cv2.waitKey(20) & 0xFF == ord('q'):
                break
        else:
            break


if __name__ == "__main__":
    main()