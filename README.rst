========
Autolock
========


Program do automatycznego wylogowania nieobecnego przed komputerem użytkownika z systemu.


Description
===========

Celem aplikacji jest umożliwienie automatycznego wylogowania nieobecnego przed komputerem użytkownika z systemu. Na wykonanie takiej operacji pozwala, zastosowana w projekcie, biblioteka OpenCV. Pracą aplikacji steruje kilka algorytmów. 


Note
====

This project has been set up using PyScaffold 2.4.4. For details and usage
information on PyScaffold see http://pyscaffold.readthedocs.org/.
